#double
nextflow run main.nf --longMode --shortMode --s -with-singularity variphyer.sif
nextflow run main.nf --longMode --shortMode --r -with-singularity variphyer.sif
nextflow run main.nf --longMode --shortMode --a -with-singularity variphyer.sif

#errors
nextflow run main.nf --longMode --a --s
nextflow run main.nf

#complete
nextflow run main.nf -resume --longMode --shortMode --s --spadesIllumina -with-dag flowchart.png -with-timeline timeline.html -with-singularity variphyerContainer.sif
nextflow run main.nf --longMode --shortMode --s -with-dag flowchart.png -with-timeline timeline.html -with-singularity variphyerContainer.sif

nextflow run -resume main.nf --longMode --shortMode --s -with-dag flowchart.png -with-timeline timeline.html