

sed -i 's/_//g' originalB.tree
sed -i 's/.contigs.fasta//g' trees_7_26/originalB.tree

sed -i 's/_//g' trees_7_26/beast_*/out.txt

sed -i 's/.contigs.fasta//g' trees_7_26/beast_*/out.txt
sed -i 's/.contigs.fa//g' trees_7_26/beast_*/out.txt
sed -i 's/.fasta//g' trees_7_26/beast_*/out.txt
sed -i 's/.fa//g' trees_7_26/beast_*/out.txt
sed -i 's/reference.ref/reference/g' trees_7_26/beast_*/out.txt
sed -i 's/GCF000240185.1ASM24018v2genomic.fna.ref/reference/g' trees_7_26/beast_*/out.txt


sed -i 's/_//g' trees_7_26/raxml_*/RAxML_bestTree._raxml

sed -i 's/.contigs.fasta//g' trees_7_26/raxml_*/RAxML_bestTree._raxml
sed -i 's/.contigs.fa//g' trees_7_26/raxml_*/RAxML_bestTree._raxml
sed -i 's/.fasta//g' trees_7_26/raxml_*/RAxML_bestTree._raxml
sed -i 's/.fa//g' trees_7_26/raxml_*/RAxML_bestTree._raxml
sed -i 's/reference.ref/reference/g' trees_7_26/raxml_*/RAxML_bestTree._raxml
sed -i 's/GCF000240185.1ASM24018v2genomic.fna.ref/reference/g' trees_7_26/raxml_*/RAxML_bestTree._raxml