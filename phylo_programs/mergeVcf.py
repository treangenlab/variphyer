#!/usr/bin/env python

import io
import os
import pandas as pd

from os import listdir
from os.path import isfile, join
import sys
mypath = sys.argv[1]
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
numFiles = len(onlyfiles)

for i in range(numFiles):
    with open(mypath + '/' + onlyfiles[i], 'r') as f:
        lines = [l for l in f if not l.startswith('##')]
        dfNext = pd.read_csv(
            io.StringIO(''.join(lines)),
            dtype={'#CHROM': str, 'POS': int, 'ID': str, 'REF': str, 'ALT': str,
                    'QUAL': str, 'FILTER': str, 'INFO': str},
            sep='\t'
        ).drop(columns=['QUAL', 'INFO'])
        dfNext.insert(6, onlyfiles[i], '1')

    if(i == 0): # first file, initialize the dfPrev
        dfPrev = dfNext
    else:
        dfPrev = pd.merge(
            dfPrev,
            dfNext,
            on=['#CHROM', 'POS', 'ID', 'REF', 'ALT', 'FILTER'],
            #on=['#CHROM', 'POS', 'ALT'],
            how="outer",
            sort=True
        )
for i in range(numFiles):
    dfPrev[onlyfiles[i]] = dfPrev[onlyfiles[i]].fillna('0')

# QUAL = 40 (Random number), INFO = NA, FORMAT = GT
dfPrev.insert(5, 'QUAL', 40)
dfPrev.insert(7, 'INFO', 'NA')
dfPrev.insert(8, 'FORMAT', 'GT')
#remove some qual columns
dfPrev.to_csv(sys.argv[2]+'/out.vcf',index=False, sep = '\t')

