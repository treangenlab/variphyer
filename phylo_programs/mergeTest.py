import pandas as pd


left = pd.DataFrame(
     {
        "key1": ["K0", "K0", "K1", "K2"],
        "key2": ["K0", "K1", "K0", "K1"],
        #"A": ['1','1', '1', '1'],
    }
)
left.insert(2, 'A', '1')
  

right = pd.DataFrame(
    {
        "key1": ["K0", "K1", "K1", "K2"],
        "key2": ["K0", "K0", "K0", "K0"],
        #"C": ['1', '1', '1', '1'],
    }
)
right.insert(2, 'B', '1')

result = pd.merge(left, right, how="outer", on=["key1", "key2"])

result['A'] = result['A'].fillna('0')
print(result)