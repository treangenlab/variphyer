from Bio import AlignIO
import sys

input_handle = open(sys.argv[1], "r")
output_handle = open(sys.argv[2], "w")

alignments = AlignIO.parse(input_handle, "mauve")
AlignIO.write(alignments, output_handle, "phylip")

output_handle.close()
input_handle.close()