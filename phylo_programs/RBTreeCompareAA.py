import dendropy
from dendropy.calculate import treecompare
import numpy as np
import pandas as pd
import sys

tns = dendropy.TaxonNamespace()

treeOriginal = dendropy.datamodel.treemodel.Tree.get_from_path(
    sys.argv[1],
    "newick",
    taxon_namespace=tns)

treeRaxmlSpadesHybridParsnp = dendropy.datamodel.treemodel.Tree.get_from_path(
    "raxmlSpadesHybridParsnp",
    "newick",
    taxon_namespace=tns)
treeRaxmlSpadesHybridMauve = dendropy.datamodel.treemodel.Tree.get_from_path(
    "raxmlSpadesHybridMauve",
    "newick",
    taxon_namespace=tns)
treeRaxmlSpadesParsnp = dendropy.datamodel.treemodel.Tree.get_from_path(
    "raxmlSpadesParsnp",
    "newick",
    taxon_namespace=tns)
treeRaxmlSpadesMauve = dendropy.datamodel.treemodel.Tree.get_from_path(
    "raxmlSpadesMauve",
    "newick",
    taxon_namespace=tns)
treeRaxmlMegahitParsnp = dendropy.datamodel.treemodel.Tree.get_from_path(
    "raxmlMegahitParsnp",
    "newick",
    taxon_namespace=tns)
treeRaxmlMauvePacbio = dendropy.datamodel.treemodel.Tree.get_from_path(
    "raxmlMauvePacbio",
    "newick",
    taxon_namespace=tns)
treeRaxmlParsnpPacbio = dendropy.datamodel.treemodel.Tree.get_from_path(
    "raxmlParsnpPacbio",
    "newick",
    taxon_namespace=tns)

treeFasttreeSpadesHybridParsnp = dendropy.datamodel.treemodel.Tree.get_from_path(
    "fasttreeSpadesHybridParsnp",
    "newick",
    taxon_namespace=tns)
treeFasttreeSpadesHybridMauve = dendropy.datamodel.treemodel.Tree.get_from_path(
    "fasttreeSpadesHybridMauve",
    "newick",
    taxon_namespace=tns)
treeFasttreeSpadesParsnp = dendropy.datamodel.treemodel.Tree.get_from_path(
    "fasttreeSpadesParsnp",
    "newick",
    taxon_namespace=tns)
treeFasttreeSpadesMauve = dendropy.datamodel.treemodel.Tree.get_from_path(
    "fasttreeSpadesMauve",
    "newick",
    taxon_namespace=tns)
treeFasttreeMegahitParsnp = dendropy.datamodel.treemodel.Tree.get_from_path(
    "fasttreeMegahitParsnp",
    "newick",
    taxon_namespace=tns)
treeFasttreeMauvePacbio = dendropy.datamodel.treemodel.Tree.get_from_path(
    "fasttreeMauvePacbio",
    "newick",
    taxon_namespace=tns)
treeFasttreeParsnpPacbio = dendropy.datamodel.treemodel.Tree.get_from_path(
    "fasttreeParsnpPacbio",
    "newick",
    taxon_namespace=tns)

treeOriginal.encode_bipartitions()
treeRaxmlSpadesHybridParsnp.encode_bipartitions()
treeRaxmlSpadesHybridMauve.encode_bipartitions()
treeRaxmlSpadesParsnp.encode_bipartitions()
treeRaxmlSpadesMauve.encode_bipartitions()
treeRaxmlMegahitParsnp.encode_bipartitions()
treeRaxmlMauvePacbio.encode_bipartitions()
treeRaxmlParsnpPacbio.encode_bipartitions()

treeFasttreeSpadesHybridParsnp.encode_bipartitions()
treeFasttreeSpadesHybridMauve.encode_bipartitions()
treeFasttreeSpadesParsnp.encode_bipartitions()
treeFasttreeSpadesMauve.encode_bipartitions()
treeFasttreeMegahitParsnp.encode_bipartitions()
treeFasttreeMauvePacbio.encode_bipartitions()
treeFasttreeParsnpPacbio.encode_bipartitions()

# dictionary
treeDict = {}
treeDict["RSpadesHybridParsnp"] = treeRaxmlSpadesHybridParsnp
treeDict["RSpadesHybridMauve"] = treeRaxmlSpadesHybridMauve
treeDict["RSpadesParsnp"] = treeRaxmlSpadesParsnp
treeDict["RSpadesMauve"] = treeRaxmlSpadesMauve
treeDict["RMegahitParsnp"] = treeRaxmlMegahitParsnp
treeDict["RMauvePacbio"] = treeRaxmlMauvePacbio
treeDict["RParsnpPacbio"] = treeRaxmlParsnpPacbio

treeDict["FSpadesHybridParsnp"] = treeFasttreeSpadesHybridParsnp
treeDict["FSpadesHybridMauve"] = treeFasttreeSpadesHybridMauve
treeDict["FSpadesParsnp"] = treeFasttreeSpadesParsnp
treeDict["FSpadesMauve"] = treeFasttreeSpadesMauve
treeDict["FMegahitParsnp"] = treeFasttreeMegahitParsnp
treeDict["FMauvePacbio"] = treeFasttreeMauvePacbio
treeDict["FParsnpPacbio"] = treeFasttreeParsnpPacbio

'''
# beast tree
treeOriginalB = dendropy.datamodel.treemodel.Tree.get_from_path(
    "originalB.nexus",
    "nexus",
    taxon_namespace=tns)
treeBeastSpadesHybridParsnp = dendropy.datamodel.treemodel.Tree.get_from_path(
    "beastSpadesHybridParsnp",
    "nexus",
    taxon_namespace=tns)
treeBeastSpadesHybridMauve = dendropy.datamodel.treemodel.Tree.get_from_path(
    "beastSpadesHybridMauve",
    "nexus",
    taxon_namespace=tns)
treeBeastSpadesParsnp = dendropy.datamodel.treemodel.Tree.get_from_path(
    "beastSpadesParsnp",
    "nexus",
    taxon_namespace=tns)
treeBeastSpadesMauve = dendropy.datamodel.treemodel.Tree.get_from_path(
    "beastSpadesMauve",
    "nexus",
    taxon_namespace=tns)
treeBeastMegahitParsnp = dendropy.datamodel.treemodel.Tree.get_from_path(
    "beastMegahitParsnp",
    "nexus",
    taxon_namespace=tns)
treeBeastMauvePacbio = dendropy.datamodel.treemodel.Tree.get_from_path(
    "beastMauvePacbio",
    "nexus",
    taxon_namespace=tns)
treeBeastParsnpPacbio = dendropy.datamodel.treemodel.Tree.get_from_path(
    "beastParsnpPacbio",
    "nexus",
    taxon_namespace=tns)

treeOriginalB.encode_bipartitions()
treeBeastSpadesHybridParsnp.encode_bipartitions()
treeBeastSpadesHybridMauve.encode_bipartitions()
treeBeastSpadesParsnp.encode_bipartitions()
treeBeastSpadesMauve.encode_bipartitions()
treeBeastMegahitParsnp.encode_bipartitions()
treeBeastMauvePacbio.encode_bipartitions()
treeBeastParsnpPacbio.encode_bipartitions()

# dictionary
treeDict["BSpadesHybridParsnp"] = treeBeastSpadesHybridParsnp
treeDict["BSpadesHybridMauve"] = treeBeastSpadesHybridMauve
treeDict["BSpadesParsnp"] = treeBeastSpadesParsnp
treeDict["BSpadesMauve"] = treeBeastSpadesMauve
treeDict["BMegahitParsnp"] = treeBeastMegahitParsnp
treeDict["BMauvePacbio"] = treeBeastMauvePacbio
treeDict["BParsnpPacbio"] = treeBeastParsnpPacbio
'''
# compare every Raxml tree with the original tree
treeSize = len(treeDict.keys())

RPd = pd.DataFrame(np.zeros((4, treeSize)), columns=[
'RSpadesHybridParsnp',
'RSpadesHybridMauve',
'RSpadesParsnp',
'RSpadesMauve',
'RMegahitParsnp',
'RMauvePacbio',
'RParsnpPacbio',
'FSpadesHybridParsnp',
'FSpadesHybridMauve',
'FSpadesParsnp',
'FSpadesMauve',
'FMegahitParsnp',
'FMauvePacbio',
'FParsnpPacbio'
],
index=['euclidean_distance', 'false_positives', 'false_negatives', 'unweighted Robinson-Foulds'])
for k, tree in treeDict.items():
    RPd.loc['euclidean_distance'][k] = treecompare.euclidean_distance(treeOriginal, tree, is_bipartitions_updated=True)
    RPd.loc['false_positives'][k] = treecompare.false_positives_and_negatives(treeOriginal, tree, is_bipartitions_updated=True)[0]
    RPd.loc['false_negatives'][k] = treecompare.false_positives_and_negatives(treeOriginal, tree, is_bipartitions_updated=True)[1]
    RPd.loc['unweighted Robinson-Foulds'][k] = treecompare.symmetric_difference(treeOriginal, tree, is_bipartitions_updated=True)
    
RPd.to_csv(sys.argv[2])
'''
'BSpadesHybridParsnp',
'BSpadesHybridMauve',
'BSpadesParsnp',
'BSpadesMauve',
'BMegahitParsnp',
'BMauvePacbio',
'BParsnpPacbio'
'''